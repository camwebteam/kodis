<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> | Cochlear </title>
<meta name="description" content="">
<!-- <link rel="shortcut icon" href="/wps/wcm/connect/d6834128-4924-44fa-8627-63d67a14d58a/favicon.ico?MOD=AJPERES"> -->
<link rel="shortcut icon" href="images//favicon.ico">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link href="css/global.css" rel="stylesheet">
<script src="//app-lon04.marketo.com/js/forms2/js/forms2.min.js"></script>
<!-- Google Tag Manager - before closing head tag-->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PP9T5H');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager -after body tag-->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PP9T5H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->

    <header>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                </div>
            </div>
        </div>
    </header>
  
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                </div>
            </div>
        </div>       
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <ul>
                        <li><a href="/wps/wcm/connect/us/privacy-policy?contentIDR=48d9463f-d782-44d4-a739-dfadebd5deb6&amp;useDefaultText=0&amp;useDefaultDesc=0" title="" target="_blank">Privacy Policy</a></li>
                        <li><a href="/wps/wcm/connect/us/terms-of-use?contentIDR=d9c844a9-c72e-4267-b53b-75d608890a5f&amp;useDefaultText=0&amp;useDefaultDesc=0" title="" target="_blank">Terms of use</a></li>
                    </ul>
                    <p class="legal">Copyright &copy; 2017 Cochlear Ltd. All rights reserved.</p>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <ul class="nav nav-pills social-media" role="tablist">
                        <li role="presentation"><a target="_blank" href="https://www.facebook.com/Cochlear" title="" class="sm-facebook"><span class="sr-only">Facebook - Opens in new window</span></a></li>
                        <li role="presentation"><a target="_blank" href="http://twitter.com/cochlearUS" title="" class="sm-twitter"><span class="sr-only">Twitter - Opens in new window</span></a></li>
                        <li role="presentation"><a target="_blank" href="http://www.youtube.com/cochlearamericas" title="" class="sm-youtube"><span class="sr-only">YouTube - Opens in new window</span></a></li>
                        <li role="presentation"><a target="_blank" href="http://www.linkedin.com/company/cochlear" title="" class="sm-linkedin"><span class="sr-only">LinkedIn - Opens in new window</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Craxy Egg -->
    <script src="http://script.crazyegg.com/pages/scripts/0032/9955.js?411762" async="" type="text/javascript"></script>
    <script type="text/javascript">
        setTimeout(function(){var a=document.createElement("script"); var b=document.getElementsByTagName("script")[0]; a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0032/9955.js?"+Math.floor(new Date().getTime()/3600000); a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
    </script>


<!-- Bootstrap core JavaScript
================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
<script src="js/global.js"></script>

<!-- fall back for css -->
<script>
    (function($) {
        $(function() {
            if ($('body').css('color') !== 'rgb(51, 51, 51)') {
                $('head').prepend('<link rel="stylesheet" href="css/bootstrap.min.css">');
            }
        });
    })(window.jQuery);
</script>

<!-- Script for local environment only -->
<script src="js/devAssetPath.js"></script>
</body>
</html>


