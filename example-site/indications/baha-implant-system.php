
<!DOCTYPE html>
<html lang="en">
<head>
<?php include "includes/header-scripts.php"; ?>
<title>Cochlear Baha Indications | Cochlear </title>
<meta name="description" content="">
<link href="css/baha-product.css" rel="stylesheet">
</head>
<body id="indications-baha">
 <!-- Google Tag Manager -after body tag -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PP9T5H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->

    <?php include "includes/header.php"; ?>

    <section id="hero">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-5">
                    <h1>The Cochlear&trade; Baha<sup>&reg;</sup> 5 Implant System.</h1>
                    <p>Life has so much more to offer with the Baha 5 Implant System.  It gives you the freedom and confidence to listen to music, enjoy a movie, eat out at a noisy restaurant, go to a concert and socialize with your friends - without worrying about missing out.</p> 
                    <p>Discover all of the incredibly smart features designed to help make listening easier.</p>
                </div>
                <div class="col-xs-12">
                    <p class="name">Shelly S., Baha recipient</p>
                </div>
            </div>            
        </div>
    </section>

    <section id="smart-choice">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Smart choices for life.</h2>
                    <p>Because your hearing needs are unique and may even change over time, you will need options for today and the future. The Baha 5 System offers three head-worn sound processors, each one designed to meet a different level of hearing loss up to 65 dB. From the smallest to the most powerful, you'll have peace of mind knowing that all Baha 5 Sound Processors feature our most advanced technologies designed to help you hear your best in any environment.</p>
                    <img src="/wps/wcm/connect/f3e3f88d-6f07-4cb3-bf6c-c9f6f66b0562/baha-lineup.jpg?MOD=AJPERES" alt="Baha 5 products" class="img-responsive">
                </div>

            <!-- ******** ######## TABS CONTENT START  ######## *** -->

            <div class="tabs" class="col-xs-12">
                <div class="tab-btn baha-5 col-xs-12 col-xs-4">
                    <h3>Baha 5<br>
                        Sound Processor</h3>
                    <p class="indicators">+</p>
                </div>
                <div class="tab-btn baha-5-power col-xs-12 col-xs-4">
                    <h3>Baha 5 Power<br>
                        Sound Processor</h3>
                    <p class="indicators">+</p>
                </div>
                <div class="tab-btn baha-5-sp col-xs-12 col-xs-4">
                    <h3>Baha 5 SuperPower<br>
                        Sound Processor</h3>
                    <p class="indicators">+</p>
                </div>
            </div>
               
            <div id="baha-5" class="tab-content baha-5-content col-xs-12">              
                <h2>Baha 5 Sound Processor</h2>
                <img src="/wps/wcm/connect/7d4c992e-51e1-4c1d-b32c-0e44a5ff0aef/banner-baha-5.jpg?MOD=AJPERES" alt="Baha 5 Sound Processor" class="img-responsive banner">
                <div class="col-xs-12 pad intro">
                    <div class="col-xs-3 col-xs-offset-1">
                        <img src="/wps/wcm/connect/e9746375-4a82-4c1f-893d-27ae544a83d1/baha-5.jpg?MOD=AJPERES" alt="Baha 5 Sound Processor" class="img-responsive">
                    </div>
                    <div class="col-xs-7 col-xs-offset-1">
                        <h3>Sometimes the biggest miracles come in the smallest packages.</h3>
                        <p>You want to hear better, but don't necessarily want everyone to know about it. The good news is that the award-winning Cochlear&trade; Baha<sup>&reg;</sup> 5 Sound Processor is 20 percent smaller than all other comparable sound processors,<sup>1</sup> yet it is full of advanced hearing technologies designed to help you hear better than ever before.</p>
                    </div>
                </div>
                <div class="col-xs-12 pad colors">
                    <h3>Choose from a range of color options.</h3>
                    <img src="/wps/wcm/connect/b5c80e6f-3bac-4507-a434-9bf941de626e/baha-colors.jpg?MOD=AJPERES" alt="Baha Colors" class="img-responsive">
                </div>
                <div id="player8"></div>
                <p class="button"><a class="gold-btn baha-indications" data-toggle="modal" data-target="#rfi-modal" data-productname="baha-5-rfi"><span class="glyphicon glyphicon-chevron-right"></span>Request Information Guide</a></p>
                <p class="legal">1. Measured as a height comparison between the Baha 5 Sound Processor (26 mm) and the smallest available competing prodcut (34mm).</p>
            </div>

            <div id="baha-5-power" class="tab-content baha-5-power-content col-xs-12">
                <h2>Baha 5 Power Sound Processor</h2>
                <img src="/wps/wcm/connect/5875c71c-9914-4317-a76d-f9bd55e5e917/banner-baha-power.jpg?MOD=AJPERES" alt="Baha Power 5 Sound Processor" class="img-responsive banner">
                <div class="col-xs-12 pad intro">
                    <div class="col-xs-3 col-xs-offset-1">
                        <img src="/wps/wcm/connect/ddd0e9c5-7c71-4fc9-aea2-0a58e402f400/baha-5-power.jpg?MOD=AJPERES" alt="Baha 5 Power Sound Processor" class="img-responsive">
                    </div>
                    <div class="col-xs-7 col-xs-offset-1">
                        <h3>Giving you the power to hear better when you need it the most.</h3>
                        <p>Created for those who need additional amplification because of a greater degree of hearing loss—up to 55dB—the Cochlear&trade; Baha<sup>&reg;</sup> 5 Power Sound Processor is designed to give you the level of sound you need with 30% less distortion than other bone conduction systems.&sup1; It provides added control by featuring a volume rocker so you can easily adjust the volume along with an LED indicator light that tells you if the sound processor is working properly. It also has a long battery life, providing up to 160 hours*, so you can have greater peace of mind that you'll have access to sound throughout your day.</p>
                        <p class="legal">*Battery life may vary by individual.</p>
                        <p class="legal">&sup1; Flynn MC. Smart and Small – innovative technologies behind the Cochlear Baha 5 Sound Processor. Cochlear Bone Anchored Solutions AB, 629761, 2015.</p>
                    </div>
                </div>
                <div class="col-xs-12 pad colors">
                    <h3>Choose from a variety of color options.</h3>
                    <img src="/wps/wcm/connect/15b9cb68-516c-47b4-a625-191016eec129/baha-power-colors.jpg?MOD=AJPERES" alt="Baha 5 Power Colors" class="img-responsive">
                </div>
                <div id="player9"></div>
                <p class="button"><a class="gold-btn baha-indications" data-toggle="modal" data-target="#rfi-modal" data-productname="baha-5-power-rfi"><span class="glyphicon glyphicon-chevron-right"></span>Request Information Guide</a></p>
            </div>

            <div id="baha-5-sp" class="tab-content baha-5-sp-content col-xs-12">
                <h2>Baha 5 SuperPower Sound Processor</h2>
                <img src="/wps/wcm/connect/6e57081e-56bb-493e-a76c-0dbf492a32a6/banner-baha-sp.jpg?MOD=AJPERES" alt="Baha SuperPower 5 Sound Processor" class="img-responsive banner">
                <div class="col-xs-12 pad intro">
                    <div class="col-xs-3 col-xs-offset-1">
                        <img src="/wps/wcm/connect/0afd8fae-ca16-4e8d-8bad-8d1e50dd582e/baha-5-sp.jpg?MOD=AJPERES" alt="Baha 5 SuperPower Sound Processor" class="img-responsive">
                    </div>
                    <div class="col-xs-7 col-xs-offset-1">
                        <h3>Our most powerful bone conduction solution to fit your needs.</h3>
                        <p>Created for those with a substantial degree of hearing loss—up to 65 dB—the Cochlear&trade; Baha<sup>&reg;</sup> 5 SuperPower Sound Processor is the industry's first head-worn super power bone conduction solution. As the most powerful member of the Baha family of sound processors, it is designed to help those struggling to understand conversations even with power hearing aids.</p>
                        <p>The Baha 5 SuperPower Sound Processor combines the best of Cochlear's innovative hearing implant technologies to give you the amount of amplification you need and your best hearing performance.</p>
                    </div>
                </div>
                <div class="col-xs-12 pad colors">
                    <h3>Choose from a range of color options to match your style.</h3>
                    <img src="/wps/wcm/connect/76471f3c-bcc1-43dd-93aa-0912360376bd/baha-sp-colors.jpg?MOD=AJPERES" alt="Baha 5 SuperPower Colors" class="img-responsive">
                </div>
                <div id="player10"></div>
                <p class="button"><a class="gold-btn baha-indications" data-toggle="modal" data-target="#rfi-modal" data-productname="baha-5-super-power-rfi"><span class="glyphicon glyphicon-chevron-right"></span>Request Information Guide</a></p>
            </div>
            <!-- ******** ####### END TABS ####### ******* -->

            </div>            
        </div>
    </section>

    <section id="one-implant" class="grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>One Implant. Two Systems.</h2>
                    <p>There are two types of Baha Systems - the Baha Attract System and the Baha Connect System. Both offer unique Cochlear technology designed to help you hear and communicate with confidence.</p>
                    <p>There are three components of every Baha System: the implant, an abutment or magnetic attachment and a sound processor.</p>
                </div>
                <div class="col-xs-12 col-sm-6 attract">
                    <img src="/wps/wcm/connect/aa45a50d-ed6e-4772-acec-7d2dae989b69/system-attract.jpg?MOD=AJPERES" alt="The Baha Attract System" class="img-responsive">
                    <p>Your invisible connection to hearing.</p>
                    <p class="button"><a class="gold-btn" data-toggle="modal" data-target="#attract-modal"><span class="glyphicon glyphicon-chevron-right"></span>Learn More</a></p>
                </div>
                <div class="col-xs-12 col-sm-6 direct">
                    <img src="/wps/wcm/connect/60fa0e40-9747-4b1a-ad99-226a4f3fda88/system-connect.jpg?MOD=AJPERES" alt="The Baha Connect System" class="img-responsive">
                    <p>Your direct connection to hearing</p>
                    <p class="button"><a class="gold-btn" data-toggle="modal" data-target="#direct-modal"><span class="glyphicon glyphicon-chevron-right"></span>Learn More</a></p>
                </div>
                <div class="col-xs-12 watch-videos">
                    <h3>Watch videos of how the Baha Systems work.</h3>
                </div>
                 <div class="col-xs-12 col-sm-6">
                    <div class="video">
                        <div class="video-holder"></div>
                        <img src="/wps/wcm/connect/378dbf6a-f40c-47ba-b77b-6726b5d00751/video-attract.jpg?MOD=AJPERES" alt="How the Baha System Works" class="img-responsive">
                        <div class="btn-play">
                            <div class="video-id">1IdcXl5gBvM</div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="video">
                        <div class="video-holder"></div>
                        <img src="/wps/wcm/connect/bde4f7f2-8fee-4206-a940-8d00ca29260c/video-connect.jpg?MOD=AJPERES" alt="How the Baha System Works" class="img-responsive">
                        <div class="btn-play">
                            <div class="video-id">EZ-M9AxesVc</div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </section>


    <section id="tech">
        <div class="container">
            <div class="row">
            <div class="col-xs-12">
                <h2>Advanced technology for a lifetime of better hearing.</h2>
                <br>
                </div>
                <div class="full-width">
                    <div class="col-xs-12 gold-bg">
                        <div class="col-xs-3">
                            <img src="/wps/wcm/connect/43175181-dbe2-4c94-b629-d306db3c737e/iq.jpg?MOD=AJPERES" alt="SmartSound IQ" class="img-responsive">
                        </div>
                        <div class="col-xs-9">
                            <h2>Our sound processors work hard to make hearing easier.</h2>
                            <p>Our innovative SmartSound<sup>&reg;</sup> iQ technology helps you achieve your best possible hearing at home, work and everywhere in between.</p>
                            <p class="button"><a class="grey-btn" data-toggle="modal" data-target="#iq-modal"><span class="glyphicon glyphicon-chevron-right"></span>Learn More</a></a></p>
                        </div>
                    </div>

                    <div class="col-xs-12 grey-bg">
                        <div class="col-xs-3">
                            <img src="/wps/wcm/connect/beeb9c60-d1a6-47c7-b644-0164e3496a35/wireless.jpg?MOD=AJPERES" alt="Cochlear True Wireless" class="img-responsive">
                        </div>
                        <div class="col-xs-9">
                            <h2>Cochlear True Wireless&trade; technology brings you closer to sound.</h2>
                            <p>Baha 5 Sound Processors are compatible with our True Wireless<sup>TM</sup> accessories that stream sound and speech to the sound processor from your TV, smartphone or GPS without anything around your neck.</p>
                            <p class="button"><a class="grey-btn" data-toggle="modal" data-target="#wireless-modal"><span class="glyphicon glyphicon-chevron-right"></span>Learn More</a></a></p>
                        </div>
                    </div>
                </div>
                <div class="two-col">
                    <div class="grey-bg">
                        <h2>Made for iPhone</h2>
                        <p>Sound processors that provide a smart connection to the people and devices you love.</p>
                        <img src="/wps/wcm/connect/6e0bd1fc-5520-4937-bcfc-622fc170efb3/iphone.jpg?MOD=AJPERES" alt="Made for iPhone" class="img-responsive">
                        <p class="button"><a class="gold-btn" data-toggle="modal" data-target="#iphone-modal"><span class="glyphicon glyphicon-chevron-right"></span>Learn More</a></a></p>
                    </div>


                    <div class="grey-bg">
                        <h2>Baha 5 Smart App</h2>
                        <p>Technology that lets you control and personalize your hearing experience.</p>
                        <img src="/wps/wcm/connect/a043ef31-5a13-4218-85bb-ab68d0eb161a/smart-app.jpg?MOD=AJPERES" alt="Baha 5 Smart App" class="img-responsive">
                        <p class="button"><a class="gold-btn" data-toggle="modal" data-target="#app-modal"><span class="glyphicon glyphicon-chevron-right"></span>Learn More</a></a></p>
                    </div>
                </div>
            </div>            
        </div>
    </section>

    <section id="learn" class="gold-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Learn more about the incredibly smart Cochlear Baha 5 Implant System.</h2>
                    <p class="button"><a class="white-btn" data-toggle="modal" data-target="#rfi-modal"><span class="glyphicon glyphicon-chevron-right"></span>Request Resource Guide</a></a></p>
                </div>
            </div>            
        </div>
    </section>

    <section id="references">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>REFERENCES</h3>
                    <ol>
                        <li>Gustafsson J. BCDrive performance vs. conventional bone conduction transducer. Cochlear Bone Anchored Solutions AB, 629908, 2015.</li>
                        
                    </ol>
                </div>
            </div>            
        </div>
    </section>

    <div class="modal fade" id="rfi-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            <h2>Find Out More</h2>
                        </div>
                        <div class="modal-body">
                            <form id="mktoForm_9366"> </form>
                            <script>
                                MktoForms2.loadForm("//app-lon04.marketo.com", "087-KYD-238", 9366);
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="attract-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            <h2>The Baha<sup>&reg;</sup> Attract System is your invisible connection to hearing.</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>The Cochlear&trade; Baha<sup>&reg;</sup> Attract System uses internal and external magnets that attract to each other to offer an invisible link between the implant and sound processor. It is a comfortable and easy to use hearing system that requires some daily care. The sound processor is easy to handle and simply snaps on to the sound processor magnet to hold it in place.</p>
                                    <img src="/wps/wcm/connect/6bd1ca1e-fad2-4def-86bb-be1c14440d05/attract-banner.jpg?MOD=AJPERES" alt="Baha Attract System" class="img-responsive">
                                    <h2>Magnets designed for comfort and lifestyle.</h2>
                                    <p>The Baha Sound Processor magnets are available in different strengths to suit your individual needs and lifestyle. Skin thickness and other factors—such as hair thickness and skin compression—will impact your required magnet strength. Choosing a comfortable sound processor magnet that provides firm retention is important for optimal hearing outcomes.</p>
                                    <p>For added comfort and performance, the sound processor magnet features a unique Baha SoftWear&trade; Pad that adapts comfortably to the shape of your head and distributes pressure evenly across the surface.</p>
                                    <h2>Designed with MRI safety in mind.*</h2>
                                    <p>Magnetic Resonance Imaging (MRI) is a diagnostic medical procedure that provides a detailed image of the internal organs and tissues. In the event you need an MRI, you can rest easier knowing that the Baha Attract System's internal magnet is approved for up to 1.5 Tesla.  For MRI scans above 1.5T, a simple procedure to remove the magnet may be necessary.</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-xs-12">
                                <p class="legal">*Prior to receiving an MRI, please consult with your clinician about proper precautions.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="direct-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            <h2>Maximize your hearing experience with the Baha<sup>&reg;</sup> Connect System.</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>The Cochlear&trade; Baha<sup>&reg;</sup> Connect System uses a small abutment that offers a direct connection between the implant and sound processor and is designed to maximize hearing performance should you need it. It is a straightforward and easy-to-use hearing system that requires little daily care. The sound processor is easy to handle and simply snaps on to the abutment to hold it in place.</p>
                                    <p>The Baha Connect System features state of the art Dermalock&trade; technology with a unique surface coating and concave shape that preserves the hair and skin around the abutment. These abutments are designed specifically for better cosmetic outcomes. In addition, surgery time is reduced and many people heal in just a few days.&sup1;</p>
                                    <h2>Safe for MRI.*</h2>
                                    <p>The Baha Connect System is designed with a high level of MRI compatibility up to 3.0 Tesla.  All you need to do is remove your Baha Sound Processor prior to the procedure. The implant and abutment can remain in place.</p>
                                    <h2>The Baha Implant—consistent and reliable access to hearing.</h2>
                                    <div class="col-xs-12 col-sm-6">
                                        <p>The long-term predictability and success of the Baha Implant results from the creation of an active bond between the titanium implant and surrounding bone tissue—a process known as osseointegration. Titanium is truly a remarkable metal with unique properties that make it ideal for implantation due to its ability to fuse with the bone. </p>
                                        <p>To help with faster and stronger osseointegration, Cochlear designed its latest titanium implant — the BI300 Implant — with TiOblast&trade; surface technology. The TiOblast surface technology is also proven to provide long-term reliability and better sound transmission compared to other systems.<sup>2,3</sup> In fact, our latest implant — the BI300 Implant — delivers a 99.5% reliability rate** that gives you peace of mind that the implant will remain stable over time.<sup>4-12</sup></p>
                                        <p>Should your hearing needs change over time, the Baha Implant offers added flexibility to upgrade to a new sound processor without the need for additional surgery. This allows you to take advantage of Cochlear's continued advancements in technology.</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <img src="/wps/wcm/connect/96c07508-0e79-454b-a0b3-cb1453c4d2fc/connect.jpg?MOD=AJPERES" alt="Baha Connect system" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-xs-12">
                                <p class="legal">*Prior to receiving an MRI, please consult with your clinician about proper precautions.<br>
                                **This study was based on the implant survival rate. Multiple factors contribute to rates of survival, including, but not limited to implant design and surgical technique.</p>
                                <p class="legal">1. Based on Controlled Market Release (CMR). Data on file. 2013</p>
                                <p class="legal">2. Dun, C.A.J. , de Wolf, M.J.F , Wigren, S., Eeg Olofsson, M., Granstrom, G., Green, K., Flynn, M.C., Stalfors, J., Rothera, M., Mylanus, E.A.M., &amp; Cremers, C.W.R.J. (2010) Development and Multi centre Clinical Investigation of a Novel Baha Implant System. Technical and 6 Month Data. Paper presented at CI 2010, Stockholm, Sweden.</p>
                                <p class="legal">3. Sennerby L, Gottlow J, Rosengren A, Flynn M. An experimental evaluation of a new craniofacial implant using the rabbit tibia model. Part II. Biomechanical findings. Otology and Neurotology (accepted).</p>
                                <p class="legal">4. Hogsbro M, Agger A, Johansen LV. Successful loading of a bone-anchored hearing implant at 1 week after surgery. Otol Neurotol. 2017;38(2):207-211.</p>
                                <p class="legal">5. Hogsbro M, Agger A, Johansen LV. Successful loading of a bone-anchored hearing implant at two weeks after surgery: randomized trial of two surgical methods and detailed stability measurements. Otol Neurotol. 2015;36(2):e51-57.</p>
                                <p class="legal">6. den Besten CA, Stalfors J, Wigren S, Blechert JI, Flynn M, Eeg-Olofsson M, Aggarwal R, Green K, Nelissen RC, Mylanus EA, Hol MK. Stability, Survival, and Tolerability of an Auditory Osseointegrated Implant for Bone Conduction Hearing: Long-Term Follow-Up of a Randomized Controlled Trial. Otol Neurotol. 2016;37(8):1077-1083.</p>
                                <p class="legal">7. D'Eredita R, Caroncini M, Saetti R. The new Baha implant: a prospective osseointegration study. Otolaryngol Head Neck Surg. 2012;146(6):979-983.</p>
                                <p class="legal">8. Felton M, Hill-Feltham P, Bruce IA. The role of stability measurements of the Baha(R) system in children. Int J Pediatr Otorhinolaryngol. 2014;78(3):513-516.</p>
                                <p class="legal">9. Husseman J, Szudek J, Monksfield P, Power D, O'Leary S, Briggs R. Simplified bone-anchored hearing aid insertion using a linear incision without soft tissue reduction. J Laryngol Otol. 2013;127 Suppl 2:S33-38.</p>
                                <p class="legal">10. Marsella P, Scorpecci A, D'Eredita R, Della Volpe A, Malerba P. Stability of osseointegrated bone conduction systems in children: a pilot study. Otol Neurotol. 2012;33(5):797-803.</p>
                                <p class="legal">11. Wilkie MD, Chakravarthy KM, Mamais C, Temple RH. Osseointegrated hearing implant surgery using a novel hydroxyapatite-coated concave abutment design. Otolaryngol Head Neck Surg. 2014;151(6):1014-1019.</p>
                                <p class="legal">12. Wilkie MD, Lightbody KA, Salamat AA, Chakravarthy KM, Luff DA, Temple RH. Stability and survival of bone anchored hearing aid implant systems in post-irradiated patients. Eur Arch Otorhinolaryngol. 2015;272(6):1371-1376.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="iq-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            <h2>A sound processor that adapts – automatically.</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <p>Our innovative SmartSound<sup>&reg;</sup> iQ technology replicates the functions of natural hearing, which may give you crisper, clearer sound, especially when it's noisy.&sup1; This technology allows your sound processor to automatically select the appropriate program based on where you are and optimize the sound to enhance your hearing in that listening environment.</p>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div id="player3"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-xs-12">
                                <p class="legal">&sup1; Gustafsson J. BCDrive performance vs. conventional bone conduction transducer. Cochlear Bone Anchored Solutions AB, 629908, 2015.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="wireless-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            <h2>Cochlear&trade; True Wireless&trade; Technology.</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>Our True Wireless&trade; accessories help you more clearly in challenging situations by sending sound directly to the sound processor. There are no cumbersome neck-worn loops, plus the accessories are easy to use and pair. The True Wireless accessories are compatible with all Baha 5 Sound Processors.</p>
                                    <div class="col-xs-12 col-sm-6">
                                        <img src="/wps/wcm/connect/c5966769-9683-41d8-8cfa-9c51b77c147a/true-wireless-accessories.jpg?MOD=AJPERES" alt="True Wireless Accessories" class="img-responsive">
                                        <div id="player4"></div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <h3>Mini Microphone 2+</h3>
                                        <p>The Mini Microphone 2+ streams speech and music directly to your Baha 5 Sound Processor for excellent sound quality in noisy places.  Whether you are dining at a restaurant, riding in a car, in a presentation at work or shopping, you can hear more clearly in any listening situation.</p>
                                        <h3>Phone Clip</h3>
                                        <p>Talking on the phone is both important for work and connecting with loved ones. When not using the Made for iPhone connectivity or when using any other smartphone-including an Android&trade; device, the Phone Clip can help make talking on the phone and streaming sound from your GPS navigation easier.</p>
                                        <h3>TV Streamer</h3>
                                        <p>The TV Streamer lets you enjoy stereo-quality sound directly from the TV while still being able to take part in conversations around you.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-xs-12">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="iphone-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            <h2>Made for iPhone<sup>&reg;</sup></h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <p>Cochlear&trade; Baha<sup>&reg;</sup> 5 Sound Processors connect you to the people and devices you love. Unlike any other bone conduction system, Baha Sound Processors let you stream sound directly from your iPhone<sup>&reg;</sup>, iPad<sup>&reg;</sup> or iPod touch<sup>&reg;</sup>.</p>
                                    <img src="/wps/wcm/connect/7afd3f65-f51d-421b-b31f-7139a12120c5/iphone-modal.jpg?MOD=AJPERES" alt="Made for iPhone" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <p>Made for iPhone Hearing Devices are designed to provide outstanding sound quality, so you may enjoy clearer phone conversations, watch your favorite movies, listen to music on the go, and even participate in a video call with family on the other side of the world. You may hear every sound, every word clearer and crisper than before. And the best thing is no one will even know you have it on.</p>
                                    <div id="player5"></div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <p class="legal">Cochlear Baha 5 Sound Processors are compatible with iPhone 8 Plus, iPhone 8, iPhone 7 Plus, iPhone 7, iPhone 6s Plus, iPhone 6s, iPhone 6 Plus, iPhone 6, iPhone SE, iPhone 5s, iPhone 5c, iPhone 5, iPad Pro (12.9 inch), iPad Pro (9.7 inch), iPad Air 2, iPad Air, iPad mini 4, iPad mini 3, iPad mini 2, iPad mini, iPad (4th generation), iPod touch (6th generation) and iPod touch (5th generation) using iOS 7 or later.</p>
                            <p class="legal">Apple, the Apple logo, FaceTime, iPhone, iPad and iPod touch are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc.</p>
                            <p class="legal">The Bluetooth<sup>&reg;</sup> word mark and logos are registered trademarks owned by Bluetooth SIG,Inc. and any use of such marks by Cochlear is under license.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="app-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            <h2>Baha 5<sup>&reg;</sup> Smart App</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8">
                                    <p>The Baha 5 Smart App lets you personalize your hearing experience. You can quickly and easily:</p>
                                    <ul>
                                        <li>Change programs</li>
                                        <li>Adjust volume</li>
                                        <li>Check battery level</li>
                                        <li>Modify the treble and bass</li>
                                        <li>Save custom settings for certain locations</li>
                                        <li>Get help finding your child's sound processor if it is misplaced</li>
                                    </ul>
                                    <img src="/wps/wcm/connect/e310cb8c-e013-4ce9-ac0a-2ff37d80606d/app-logos.jpg?MOD=AJPERES" alt="Made for iPhone" class="img-responsive" usemap="#app-urls">
                                    <map name="app-urls">
                                        <area alt="apple" coords="0,22,160,70" href="https://itunes.apple.com/us/app/baha-5-smart/id949862838?mt=8" shape="rect" target="_blank"/>
                                        <area alt="android" coords="170,22,329,70" href="https://play.google.com/store/apps/details?id=com.cochlear.bahasmart" shape="rect" target="_blank"/>
                                    </map>                                    
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <img src="/wps/wcm/connect/a7d80b7e-0cad-4e42-b2d6-d8188be8a4be/smart-app-modal.png?MOD=AJPERES" alt="Made for iPhone" class="img-responsive">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div id="player6"></div>
                                    <p>Watch the Baha 5 Smart App in action</p>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div id="player7"></div>
                                    <p>Andria L., Baha 5 System recipient and Baha Smart App user</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-xs-12">
                                <p class="legal">The Cochlear Baha 5 Smart App works together with all Cochlear Baha 5 sound processors.It is verified on iPhone, iPad and iPod touch running iOS 9.1 and Samsung Galaxy S6 and S7 running Android OS 5.0 Lollipop. For more detailed device compatibility see the Baha 5 Smart App description on App Store or Google Play.</p>
                                <p class="legal">It is verified on iPhone, iPad and iPod touch running iOS 9.1. For more detailed device compatibility see the Baha 5 Smart App description on App Store.</p>
                                <p class="legal">Apple, the Apple logo, FaceTime, iPhone, iPad and iPod touch are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include "includes/footer.php"; ?>

    <script src="js/tabs.js"></script>

    <script>
        $(document).ready(function() {

            //URL Parsing logic
            var currentURL = window.location.href;
            var parmName = /modal=/;
            var parmTab = /tab=/;

            var hasParm = currentURL.search(parmName);
            var hasTab = currentURL.search(parmTab);

            if (hasParm >= 0) {
                var splitURL = currentURL.split('modal=');
                var firstSplitValue = splitURL[1];
                var firstString = firstSplitValue.toString();
                var secondSplit = firstString.split('&');
                var finalProduct = secondSplit[0];
                
                $('#' + finalProduct).modal('show'); 

            } else if (hasTab >= 0) {
                var splitURL = currentURL.split('tab=');
                var firstSplitValue = splitURL[1];
                var firstString = firstSplitValue.toString();
                var secondSplit = firstString.split('&');
                var finalProduct = secondSplit[0];

                $("html, body").delay(2000).animate({scrollTop: $('#smart-choice').offset().top }, 1000);

                if (finalProduct == 'baha-5') {
                    $('.baha-5').addClass('active');
                } else if (finalProduct == 'baha-5-power') {
                    $('.baha-5-power').addClass('active');
                } else {
                    $('.baha-5-sp').addClass('active');
                }
                $('#' + finalProduct).fadeIn('fast'); 
            }

            //url parameters to launch modal windows

            /* 
            // Modals:

            // modal=attract-modal
            // modal=direct-modal
            // modal=iq-modal 
            // modal=wireless-modal
            // modal=iphone-modal
            // modal=app-modal
            
            Tabs:

            tab=baha-5
            tab=baha-5-power
            tab=baha-5-sp
            */

            //Functionality to stop video when modal window is closed from either the close button or the modal gray background
            $('#iq-modal button.close, #iq-modal, #wireless-modal button.close, #wireless-modal, #iphone-modal button.close, #iphone-modal, #app-modal button.close, #app-modal, .tab-btn').click(function () {
                player3.pauseVideo();
                player4.pauseVideo();
                player5.pauseVideo();
                player6.pauseVideo();
                player7.pauseVideo();
                player8.pauseVideo();
                player9.pauseVideo();
                player10.pauseVideo();
            });
        });
    </script>

<!-- Script for local environment only -->
<script src="js/devAssetPath.js"></script>
</body>
</html>