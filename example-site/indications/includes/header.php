    <header>
        <div class="container">
            <div class="row">
                <nav id="desktop-nav">
                    <div class="container-fluid">
                        <img alt="Cochlear - Hear now. And Always" class="img-responsive" src="/wps/wcm/myconnect/b0c63801-f6f3-4198-b929-eb8156008a86/logo.png?MOD=AJPERES">
                        <div class="top-nav">
                            <ul>
                                <li><a href="/wps/wcm/connect/us/contact/contact-us" target="_blank">Contact Us</a></li>
                                <li><a href="/wps/wcm/connect/us/home/take-the-next-step/contact-a-hearing-specialist" target="_blank">Find a Hearing Implant Specialist</a></li>
                                <button data-toggle="modal" data-target="#rfi-modal" type="button" class="gold-btn" title="Contact Us"><span class="glyphicon glyphicon-chevron-right"></span>Download Free Brochure</button>
                                <p class="phone">Questions? Call us: 800-216-8519</p>
                            </ul>
                        </div>
                        <div class="bottom-nav">  
                            <ul class="navbar-nav">
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/index.html">Bone Conduction Systems<br>
                                and Hearing Loss</a></li>
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/single-sided-deafness.html">What is Single-Sided<br>
                                Deafness</a></li>
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/mixed-hearing-loss.html">What is Conductive/<br>
                                Mixed Hearing Loss?</a></li>
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/baha-implant-system.html">The Cochlear&trade; Baha<sup>&reg;</sup><br>
                                Implant System</a></li>
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/next-steps.html">Take the<br>
                                Next Step</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <nav id="mobile-nav">
                    <img alt="Cochlear - Hear now. And Always" class="img-responsive" src="images/logo.png">
                    <button class="mobile-nav-btn nav-closed" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="glyphicon glyphicon-menu-hamburger"></span>
                    </button>
                    <div class="mobile-nav-container">
                        <button class="mobile-nav-btn nav-open" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="glyphicon glyphicon-menu-hamburger"></span>
                        </button>
                        <div class="top-nav">
                            <ul class="navbar-nav">
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/index.html">Bone Conduction Systems<br>
                                and Hearing Loss</a></li>
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/single-sided-deafness.html">What is Single-Sided<br>
                                Deafness</a></li>
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/mixed-hearing-loss.html">What is Conductive/<br>
                                Mixed Hearing Loss?</a></li>
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/baha-implant-system.html">The Cochlear&trade; Baha<sup>&reg;</sup><br>
                                Implant System</a></li>
                                <li class="nav-item nav-link"><a href="/wps/wcm/myconnect/us/home/indications/baha-bone-conduction-implants/next-steps.html">Take the<br>
                                Next Step</a></li>
                            </ul>
                        </div>
                        <div class="bottom-nav">  
                            <p><a class="white-btn" href="/wps/wcm/connect/us/contact/contact-us"><span class="glyphicon glyphicon-chevron-right"></span>Contact Us</a></p>
                            <p><a class="white-btn" href="/wps/wcm/connect/us/home/take-the-next-step/contact-a-hearing-specialist"><span class="glyphicon glyphicon-chevron-right"></span>Find a Hearing Implant Specialist</a></p>
                            <button data-toggle="modal" data-target="#rfi-modal" type="button" class="gold-btn" title="Contact Us"><span class="glyphicon glyphicon-chevron-right"></span>Download Free Brochure</button>
                            <p>Questions? Call us: <a href="tel:800-216-8519">800-216-8519</a></p>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>