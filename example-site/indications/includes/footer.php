    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <ul>
                        <li><a href="/wps/wcm/connect/us/privacy-policy?contentIDR=48d9463f-d782-44d4-a739-dfadebd5deb6&amp;useDefaultText=0&amp;useDefaultDesc=0" title="" target="_blank">Privacy Policy</a></li>
                        <li><a href="/wps/wcm/connect/us/terms-of-use?contentIDR=d9c844a9-c72e-4267-b53b-75d608890a5f&amp;useDefaultText=0&amp;useDefaultDesc=0" title="" target="_blank">Terms of use</a></li>
                    </ul>
                    <p class="legal">Copyright &copy; 2017 Cochlear Ltd. All rights reserved.</p>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <ul class="nav nav-pills social-media" role="tablist">
                        <li role="presentation"><a target="_blank" href="https://www.facebook.com/Cochlear" title="" class="sm-facebook"><span class="sr-only">Facebook - Opens in new window</span></a></li>
                        <li role="presentation"><a target="_blank" href="http://twitter.com/cochlearUS" title="" class="sm-twitter"><span class="sr-only">Twitter - Opens in new window</span></a></li>
                        <li role="presentation"><a target="_blank" href="http://www.youtube.com/cochlearamericas" title="" class="sm-youtube"><span class="sr-only">YouTube - Opens in new window</span></a></li>
                        <li role="presentation"><a target="_blank" href="http://www.linkedin.com/company/cochlear" title="" class="sm-linkedin"><span class="sr-only">LinkedIn - Opens in new window</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

<!-- Craxy Egg -->
<script src="http://script.crazyegg.com/pages/scripts/0032/9955.js?411762" async="" type="text/javascript"></script>
<script type="text/javascript">
    setTimeout(function(){var a=document.createElement("script"); var b=document.getElementsByTagName("script")[0]; a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0032/9955.js?"+Math.floor(new Date().getTime()/3600000); a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>


<!-- Bootstrap core JavaScript
================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/bootstrap.min.js"><\/script>')</script>
<script src="js/indications.js"></script>


<!-- fall back for css -->
<script>
    (function($) {
        $(function() {
            if ($('body').css('color') !== 'rgb(51, 51, 51)') {
                $('head').prepend('<link rel="stylesheet" href="css/bootstrap.min.css">');
            }
        });
    })(window.jQuery);
</script>