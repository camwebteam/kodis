<!DOCTYPE html>
<html lang="en">
<head>
<?php include "includes/header-scripts.php"; ?>
<title>Cochlear Baha Indications | Cochlear </title>
<meta name="description" content="">
<link href="css/home.css" rel="stylesheet">
</head>
<body id="indications-home">
<!-- Google Tag Manager -after body tag -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PP9T5H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->

    <?php include "includes/header.php"; ?>

    <section id="hero">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
                    <h1>Better hearing starts here.</h1>
                    <p>When you live with hearing loss, you're not simply missing out on sounds, you're missing out on life.</p>
                    <p>If you have single-sided deafness, conductive hearing loss or mixed hearing loss, the Cochlear&trade; Baha<sup>&reg;</sup> Bone Conduction Implant System may help you hear again and reconnect you to the people you love.</p>
                </div>
                <div class="col-xs-12">
                    <p class="name">Randi L., Baha recipient</p>
                </div>
            </div>            
        </div>
    </section>

    <section id="brilliance">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>The brilliance of bone conduction.</h2>
                    <p>There are two ways we hear sound-- through air conduction and through bone conduction. They both work together to help us listen to and perceive sound. However, when we hear ourselves speak, it is actually the enhancement of lower frequencies by vibrations in the bone that helps us perceive our 'normal' tone of voice.</p>
                    <p>Bone conduction implants, like <strong>the Cochlear™ Baha® System</strong>, work on the exact same principle, only on a much more sophisticated scale. One of the first steps in understanding the benefits of bone conduction is to understand how natural hearing works and the types of hearing loss.</p>
                </div>
            </div>            
        </div>
    </section>

    <section id="how-works" class="grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h2>How hearing works.</h2>
                    <p>Hearing is the process of sound traveling through the outer, middle and inner ear where it is then interpreted by the brain.</p>
                    <p>When all parts of the ear are healthy, here's how hearing works:</p>
                    <ol>
                        <li><strong>Outer Ear:</strong> Sounds enter the ear canal and travel to the eardrum.</li>
                        <li><strong>Middle Ear:</strong> These sound waves cause the eardrum to vibrate, sending the bones in the middle ear into motion.</li>
                        <li><strong>Inner Ear:</strong> This motion is converted to electric impulses by tiny sensory hair cells inside the inner ear (cochlea).</li>
                        <li><strong>Hearing Nerve:</strong> These electric impulses are sent to the brain, where they are perceived by the listener as sound.</li>
                    </ol>
                </div>
                <div class="col-xs-12 col-sm-5  col-sm-offset-1">
                    <img src="/wps/wcm/connect/c9e14b75-16b8-403d-86c3-28dfbd926fc2/how-works.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-c9e14b75-16b8-403d-86c3-28dfbd926fc2-m09t1au" alt="How The Cochlear Baha System Works" class="img-responsive">
                </div>
            </div>            
        </div>
    </section>

    <section id="types">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 border">
                    <h2>Types of hearing loss.</h2>
                    <p>Every hearing loss journey is different, and the causes vary from person to person. Your hearing loss may affect one ear or two; and it may stem from a problem in the inner, middle or outer ear, or from a combination of places.  Depending on the degree and which part of the ear is damaged, bone conduction hearing implants may be a solution to help you access the sounds you’re missing.</p>
                    <div class="dark-gold col-xs-12">
                        <div class="col-xs-4 col-sm-2 lighter">
                            <h3>Type</h3>
                        </div>
                        <div class="col-xs-8 col-sm-10 darker">
                            <h3>Description</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 no-color-row">
                        <div class="col-xs-4 col-sm-2">
                            <p><strong>Single-Sided Deafness</strong></p>
                        </div>
                        <div class="col-xs-8 col-sm-10">
                            <p>Sensorineural hearing loss, or damage to the inner ear, can occur in one or both ears. If the loss is in one ear, it is often referred to as unilateral hearing loss. When there is little to no hearing in one ear, but normal hearing in the other ear, it is called single-sided deafness.<br>
                            <a href="/wps/wcm/connect/us/home/indications/baha-bone-conduction-implants/single-sided-deafness.html">Learn More about Single-Sided Deafness &gt;&gt;</a></p>
                        </div>
                    </div>
                    <div class="light-gold col-xs-12">
                        <div class="col-xs-4 col-sm-2">
                            <p><strong>Conductive Hearing Loss</strong></p>
                        </div>
                        <div class="col-xs-8 col-sm-10">
                            <p>Sound cannot travel through the outer or middle ear because of chronic ear infections, draining ears or other factors.<br>
                            <a href="/wps/wcm/connect/us/home/indications/baha-bone-conduction-implants/mixed-hearing-loss.html">Learn More about Conductive Hearing Loss &gt;&gt;</a></p>
                        </div>
                    </div>
                    <div class="col-xs-12 no-color-row">
                        <div class="col-xs-4 col-sm-2">
                            <p><strong>Mixed Hearing Loss</strong></p>
                        </div>
                        <div class="col-xs-8 col-sm-10">
                            <p>Refers to a combination of conductive and sensorineural hearing loss. This means there may be damage in both the outer or middle ear and the inner ear.<br>
                            <a href="/wps/wcm/connect/us/home/indications/baha-bone-conduction-implants/mixed-hearing-loss.html">Learn More Mixed Hearing Loss &gt;&gt;</a></p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </section>

    <section id="how-video" class="gold-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>How the Baha System works.</h2>
                </div>
                <div class="col-xs-12 col-md-4">
                    <p>While a hearing aid tries to push sound through the damaged part of the ear, the Baha System uses your natural ability to conduct sound through bone vibrations. Through bone conduction, sound bypasses the damaged outer or middle ear and sends clearer, more crisp sound directly to your inner ear.&sup1;</p> 
                    <p>How your body accomplishes this process will depend on whether you have single-sided deafness, conductive or mixed hearing loss.</p>
                </div>
                <div class="col-xs-12 col-md-8">
                    
                    <div class="video">
                        <div class="video-holder"></div>
                        <img src="/wps/wcm/connect/697a6c6c-eb2e-43aa-809e-524f2cd88326/video-how-it-work.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-697a6c6c-eb2e-43aa-809e-524f2cd88326-m09t1au" alt="How the Baha System Works" class="img-responsive">
                        <div class="btn-play">
                            <div class="video-id">EZ-M9AxesVc</div>
                        </div>
                    </div>
                </div>    
                <div class="col-xs-12">
                <p class="small"><br>In the United States and Canada, the placement of a bone-anchored implant is contraindicated in children under the age of 5. </p>
                </div>       
            </div>            
        </div>
    </section>

    <section id="anchored" class="wave-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>How are bone anchored hearing devices different from traditional hearing aids?</h2>
                    <div class="row white-bg">
                        <div class="col-xs-12">
                            <div class="col-xs-12 col-sm-2">
                                <img src="/wps/wcm/connect/b178b6cc-3e90-48f3-bc9a-3ef2b4291cab/round-dad.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-b178b6cc-3e90-48f3-bc9a-3ef2b4291cab-m09t1au" alt="Hear what you've been missing" class="img-responsive">
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <h3>Hear what you've been missing</h3>
                                <p>For more than 40 years, the Baha System has been helping people hear and communicate with confidence. If you have trouble getting sufficient loudness with conventional hearing aids and struggle to understand what is being said, the Baha System may be a solution for you.</p>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-12 col-sm-2">
                                <img src="/wps/wcm/connect/b4c20057-02f9-4b7b-8cbe-79f89c8c7074/round-orchestra.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-b4c20057-02f9-4b7b-8cbe-79f89c8c7074-m09t1au" alt="Clearer, crisper sound" class="img-responsive">
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <h3>Clearer, crisper sound&sup1;</h3>
                                <p>By bypassing the middle and outer ear, the Baha System sends clearer, more crisp sound directly to your inner ear.<sup>1</sup> Plus, the Baha System doesn't rest in your ear canal, so it's much more comfortable and helps reduce problems from chronic ear infections or allergies.</p>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-12 col-sm-2">
                                <img src="/wps/wcm/connect/290c7b53-bb8e-4f4a-8024-b31ad6de4b52/round-baha.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-290c7b53-bb8e-4f4a-8024-b31ad6de4b52-m09t1au" alt="Natural pathway to hearing" class="img-responsive">
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <h3>Natural pathway to hearing</h3>
                                <p>The Baha System uses your body's natural ability to conduct sound by picking up sound vibrations from the environment and transferring them to a small titanium implant inserted into the bone behind your ear. The vibrations are then sent directly through the bone to the inner ear through a process called direct bone conduction.</p>
                            </div>
                        </div>
                        <div class="blue-bg col-xs-12">
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
                                <img src="/wps/wcm/connect/1362a05d-3078-44f4-9b5f-797953158011/demo.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-1362a05d-3078-44f4-9b5f-797953158011-m09t1au" alt="Try it first, then decide." class="img-responsive">
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
                                <h2>Try it first, then decide.</h2>
                                <p>With a bone conduction system, you have the unique advantage of hearing the difference first. Visit a <a href="/wps/wcm/connect/us/home/take-the-next-step/contact-a-hearing-specialist?contentIDR=ac34e592-6261-4afd-be28-5f1812828cff&amp;useDefaultText=0&amp;useDefaultDesc=0" target="_blank" title="">Hearing Implant Specialist</a> to try (demo) a sound processor so you can hear how it might sound before you make any decisions.</p>
                            </div>
                        </div>
                    </div>
                </div>           
            </div>            
        </div>
    </section>

    <section id="cost" class="blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>How much does the Baha System cost?</h2>
                    <p>Unlike hearing aids, Cochlear™ Baha<sup>®</sup> Bone Conduction Implant Systems are covered by Medicare. They are also covered by most insurance plans and typically Medicaid.* Contact your insurance company or local <a href="/wps/wcm/connect/us/home/take-the-next-step/contact-a-hearing-specialist?contentIDR=ac34e592-6261-4afd-be28-5f1812828cff&amp;useDefaultText=0&amp;useDefaultDesc=0" target="_blank" title="">Hearing Implant Specialist</a> to determine coverage as well as your estimated out-of-pocket expenses. The cost of the bone conduction implant will vary from person to person based on their specific health plan.</p>
                    <p><a href="/wps/wcm/connect/us/home/take-the-next-step/insurance-resource-center?contentIDR=32d7363b-95c5-4cf0-a67e-d48bd701c43f&amp;useDefaultText=0&amp;useDefaultDesc=0" target="_blank" title="">Learn more about insurance coverage &gt;&gt;</a></p>
                    <p class="legal">*Covered for Medicare beneficiaries who meet CMS criteria for coverage. Coverage for adult Medicaid recipients varies according to state specific guidelines. Contact your insurance provider or hearing implant specialist to determine your eligibility for coverage.</p>
                </div>
            </div>            
        </div>
    </section>

    <section id="testimonials">
        <div class="container one-video">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="video">
                        <div class="video-holder"></div>
                        <img src="/wps/wcm/connect/8e5935d1-9445-4257-83e2-7ae99b1fe010/video-testimonial.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-8e5935d1-9445-4257-83e2-7ae99b1fe010-m09t1au" alt="How the Baha System Works" class="img-responsive">
                        <div class="btn-play">
                            <div class="video-id">ndKhdVTPP2A</div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <h2>David’s Story</h2>
                    <div class="col-xs-1 border"></div>
                    <div class="col-xs-10 content">
                        <p>David was born with Goldenhar Syndrome and has suffered from conductive hearing loss for over 60 years. After "coping" with hearing loss for many years, David finally wanted to have a chance at a better life and he knew that the Baha 5 Power Hearing implant was his chance. Learn more about David's hearing journey and how his Baha 5 Power has changed his life.</p>
                    </div>
                    <div class="col-xs-1 border"></div>
                </div>
            </div>            
        </div>
    </section>

    <section id="references">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>REFERENCES</h3>
                    <ol>
                        <li>Gustafsson J. BCDrive performance vs. conventional bone conduction transducer. Cochlear Bone Anchored Solutions AB, 629908, 2015.</li>
                    </ol>
                </div>
            </div>            
        </div>
    </section>

    <!-- Modal windows -->
    <div class="modal fade" id="rfi-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h2>Find Out More</h2>
                </div>
                <div class="modal-body">
                    <form id="mktoForm_9366"> </form>
                    <script>
                        MktoForms2.loadForm("//app-lon04.marketo.com", "087-KYD-238", 9366);
                    </script>
                </div>
            </div>
        </div>
    </div>

    <?php include "includes/footer.php"; ?>


<!-- Script for local environment only -->
<script src="js/devAssetPath.js"></script>
</body>
</html>