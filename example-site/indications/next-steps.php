<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php include "includes/header-scripts.php"; ?>
<title>Cochlear Baha Indications | Cochlear </title>
<meta name="description" content="">
<link href="css/steps.css" rel="stylesheet">
</head>
<body id="indications-steps">
<!-- Google Tag Manager -after body tag -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PP9T5H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->

    <?php include "includes/header.php"; ?>

    <section id="journey">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Your journey to hearing is worth every step.</h2>
                    <h3>Getting the Cochlear&trade; Baha<sup>&reg;</sup> Implant System.</h3>
                    <p>In all our years of helping people hear better, we've never heard someone say, "I wish I would have waited." In fact, the opposite is true. And once you make this life-changing decision, there are a few steps to get you well on your way to hearing your best.</p>
                </div>
            </div>
        </div>
    </section>  

     <section id="steps" class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">                 
                    <div id="stepsCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="col-md-4 hidden-sm hidden-xs left-step">
                                    <h2>Step 1:<br>
                                    Hearing Test</h2>
                                </div>
                                <div class="col-md-8 col-sm-12 right-step">
                                    <h2 class="hidden-lg">Step 1:<br>
                                    Hearing Test</h2>
                                    <p>You will need to visit a <a href="/wps/wcm/myconnect/us/home/take-the-next-step/contact-a-hearing-specialist?contentIDR=ac34e592-6261-4afd-be28-5f1812828cff&amp;useDefaultText=0&amp;useDefaultDesc=0" target="_blank" title="">Hearing Implant Specialist</a> to determine if you are a candidate for a bone conduction implant. If it’s determined you are a good candidate, you’ll have the unique advantage of hearing the difference first. Ask your Hearing Implant Specialist to try a sound processor so you can hear how it might sound before you make any decisions.</p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-md-4 hidden-sm hidden-xs left-step">
                                    <h2>Step 2:<br>
                                    Insurance Approval</h2>
                                </div>
                                <div class="col-md-8 col-sm-12 right-step">
                                    <h2 class="hidden-lg">Step 2:<br>
                                    Insurance Approval</h2>
                                    <p>Your <a href="/wps/wcm/myconnect/us/home/take-the-next-step/contact-a-hearing-specialist?contentIDR=ac34e592-6261-4afd-be28-5f1812828cff&amp;useDefaultText=0&amp;useDefaultDesc=0" target="_blank" title="">Hearing Implant Specialist</a> will work with your insurance company to determine your eligibility for coverage.</p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-md-4 hidden-sm hidden-xs left-step">
                                    <h2>Step 3:<br>
                                    Surgery</h2>
                                </div>
                                <div class="col-md-8 col-sm-12 right-step">
                                    <h2 class="hidden-lg">Step 3:<br>
                                    Surgery</h2>
                                    <p>This outpatient surgery is usually done under general anesthesia and typically lasts less than an hour. In a few days, most people are back to their normal activities.</p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-md-4 hidden-sm hidden-xs left-step">
                                    <h2>Step 4:<br>
                                    Fitting</h2>
                                </div>
                                <div class="col-md-8 col-sm-12 right-step">
                                    <h2 class="hidden-lg">Step 4:<br>
                                    Fitting</h2>
                                    <p>This is the best part. After the healing period (up to 12 weeks), you’ll visit your Hearing Implant Specialist and receive your new sound processor. They’ll adjust the settings to match your needs and show you how it all works. And that’s it. You’ll be ready to start your new hearing experience.</p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-md-4 hidden-sm hidden-xs left-step">
                                    <h2>Step 5:<br>
                                    Ongoing Care</h2>
                                </div>
                                <div class="col-md-8 col-sm-12 right-step">
                                    <h2 class="hidden-lg">Step 5:<br>
                                    Ongoing Care</h2>
                                    <p>Just as with any hearing device, you may need minor adjustments and to fine-tune your sound processor to ensure your best hearing experience. You will also want to regularly clean and care for your sound processor as well as your abutment site if you received the Baha Connect System.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Indicators -->
                        <ol id="video-target" class="carousel-indicators hidden-xs">
                            <li data-target="#stepsCarousel" data-slide-to="0" data-productname="step-1" class="active baha-indications">Step 1</li>
                            <li data-target="#stepsCarousel" data-slide-to="1" data-productname="step-2" class="baha-indications">Step 2</li>
                            <li data-target="#stepsCarousel" data-slide-to="2" data-productname="step-3" class="baha-indications">Step 3</li>
                            <li data-target="#stepsCarousel" data-slide-to="3" data-productname="step-4" class="baha-indications">Step 4</li>
                            <li data-target="#stepsCarousel" data-slide-to="4" data-productname="step-5" class="baha-indications">Step 5</li>
                        </ol>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#stepsCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#stepsCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="help">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>We’re here to help!</h2>
                    <p>Hearing loss affects people of all ages. It can affect how you live, learn and socialize with friends and family. That's why we've devoted over 40 years in pioneering advanced bone conduction implant solutions to help people just like you get back the sounds you're missing and live the life you want.</p>

                    <p>We understand that you may have questions throughout your journey to better hearing, and we want you to feel comfortable every step of the way. There are many ways that you can connect with us or others who have gone through a similar journey for support and to answer your questions.</p>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-sm-4">
                        <div class="cta-box connect">
                            <h3>Cochlear Connections</h3>
                            <p>Meet others who have benefited from Cochlear’s technology.</p>
                            <img src="/wps/wcm/myconnect/cde683b9-e3cc-4158-8723-eca6287a21cb/insurance-support.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-cde683b9-e3cc-4158-8723-eca6287a21cb-m08ZhuX" alt="Cochlear Connections">
                            <p><a class="gold-btn" href="http://www.cochlear.com/wps/wcm/connect/us/home/take-the-next-step/cochlear-connections" target="_blank"><span class="glyphicon glyphicon-chevron-right"></span>Get Connected</a></p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="cta-box contact">
                            <h3>Contact Us</h3>
                            <p>If you have questions, please reach out to our expert staff.</p>
                            <img src="/wps/wcm/myconnect/4ac77ec7-8847-4083-bef3-abc15296b942/question-mark.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-4ac77ec7-8847-4083-bef3-abc15296b942-m08ZhuX" alt="Contact Us">
                            <p><a class="gold-btn" href="http://www.cochlear.com/wps/wcm/connect/us/contact/contact-us" target="_blank"><span class="glyphicon glyphicon-chevron-right"></span>Learn More</a></p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="cta-box watch">
                            <h3>Watch Stories</h3>
                            <p>Watch stories of those who are living with Cochlear's Hearing Implants.</p>
                            <img src="/wps/wcm/myconnect/a107754d-1089-48ef-be67-cb232a91a40f/video-holder.png?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-a107754d-1089-48ef-be67-cb232a91a40f-m08ZhuX" alt="Watch Stories">
                            <p><a class="gold-btn" href="http://www.cochlear.com/wps/wcm/connect/us/home/stories-and-community/watch-recipient-videos" target="_blank"><span class="glyphicon glyphicon-chevron-right"></span>Watch Here</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 

    <!-- Modals -->
    <div class="modal fade" id="rfi-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h2>Find Out More</h2>
                </div>
                <div class="modal-body">
                    <form id="mktoForm_9366"> </form>
                    <script>
                        MktoForms2.loadForm("//app-lon04.marketo.com", "087-KYD-238", 9366);
                    </script>
                </div>
            </div>
        </div>
    </div>

    <?php include "includes/footer.php"; ?>

    <script>
        //stop carousel from auto rotating
        $('.carousel').carousel({
            interval: false
        }); 
    </script>
    
<!-- Script for local environment only -->
<script src="js/devAssetPath.js"></script>
</body>
</html>