<!DOCTYPE html>
<html lang="en">
<head>
<?php include "includes/header-scripts.php"; ?>
<title>Cochlear Baha Indications | Cochlear </title>
<meta name="description" content="">
<link href="css/ssd.css" rel="stylesheet">
</head>
<body id="indications-ssd">
 <!-- Google Tag Manager -after body tag -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PP9T5H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->

    <?php include "includes/header.php"; ?>

  <section id="hero">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6 col-lg-5">
                    <h1>Hear the Other Side.</h1>
                    <p>Single-sided deafness can make everyday activities more difficult. Business meetings, crossing the street, driving a car and enjoying a meal in a busy restaurant can be hard work and very stressful.</p>
                    <p>Creative coping mechanisms become a regular occurrence – sitting on a certain side of the table or room, telling people to speak into your ‘good ear,’ avoiding busy restaurants or family gatherings and asking your colleagues to constantly repeat themselves while at the office or in meetings.</p>
                    <p>It can be exhausting.</p>
                </div>
                <div class="col-xs-12">
                    <p class="name">Debra B., Baha recipient</p>
                </div>
            </div>            
        </div>
    </section>

    <section id="understanding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 intro">
                    <h2>Understanding Single-Sided Deafness.</h2>
                    <p>Single-sided deafness (SSD) occurs when you have little to no hearing in one ear and normal hearing in the other. It can happen at birth, suddenly or gradually over time.<p>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-sm-8">
                    <h3><strong>Some of the most common causes of SSD include:</strong></h3>
                    <ul>
                        <li>Acoustic Neuroma—due to the surgical removal of benign tumor(s) developing on the nerve that connects the ear to the brain</li>
                        <li>Sudden Hearing Loss—due to viral infections; Ménière's disease; adverse reaction to medications; head or ear injuries; and many other unknown reasons</li>
                        <li>Malformation at birth or missing inner ear or cochlea</li>
                    </ul>
                </div>
            </div>            
        </div>
    </section>

    <section id="two-ears" class="grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 intro">
                    <h2>Ears are a Team.</h2>
                    <p>Hearing with two ears (called binaural hearing) provides clear advantages.</p>
                    <p>Ears work as a team. Hearing and actively engaging in life is easier when using both ears. Hearing with two ears allows you to identify sounds both near and far, as well as those that occur 360 degrees around your head.&sup1; With two ears, you can better understand speech and hear more clearly. You can also better determine where a sound is coming from and hear better in noise.</p>
                    <p><strong>If one ear is taken out of the equation, it may become difficult to:</strong></p>
                </div>
                <div class="col-xs-12">
                    <div class="accordion-header">
                        <h2>Avoid the Head Shadow Effect</h2>
                        <div class="accord-indicator">
                            <p>+</p>
                        </div>
                    </div>
                    <div id="avoid" class="accordion-body">
                        <div class="col-xs-12 col-sm-6">
                            <p>Sounds that originate from your bad side will fall in the shadow of your head, so when they finally reach your good ear, it may be more difficult to hear and understand them clearly, especially in noisy environments.<sup>2</sup> This is especially true for higher-frequency sounds. Because they have shorter wavelengths, it is easier for them to be reflected by the head.&sup2;</p>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-sm-offset-1">
                            <img src="/wps/wcm/myconnect/a715c402-f4ad-4301-9421-75c20f39a08e/head-shadow-effect.png?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-a715c402-f4ad-4301-9421-75c20f39a08e-m0jIhNf" alt="Head Shadow Effect Diagram" class="img-responsive">
                        </div>
                    </div>
                    <div class="accordion-header">
                        <h2>Understand Speech in Noise</h2>
                        <div class="accord-indicator">
                            <p>+</p>
                        </div>
                    </div>
                    <div id="understand" class="accordion-body">
                        <div class="col-xs-12 col-sm-10">
                            <p>Because consonants are higher-frequency sounds that contain much of the meaning of speech, the head-shadow effect makes it difficult for people with SSD to understand speech in the presence of background noise.&sup2;</p>
                        </div>
                    </div>

                    <div class="accordion-header">
                        <h2>Locate Sound</h2>
                        <div class="accord-indicator">
                            <p>+</p>
                        </div>
                    </div>
                    <div id="locate" class="accordion-body">
                        <div class="col-xs-12 col-sm-10">
                            <p>A limited ability to discern the direction of sound is more than inconvenient – it can be dangerous. Crossing a busy street, driving through traffic or not knowing where your child is when they call your name at the playground may be very unsettling.</p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </section>

    <section id="baha" class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-lg-6">
                    <h2>The Cochlear&trade; Baha<sup>&reg;</sup> System for SSD</h2>
                    <p>The Cochlear&trade; Baha<sup>&reg;</sup> System is an implantable hearing solution that works by picking up sounds on your deaf side, converting them into sound vibrations and transferring them to your good, working ear through the skull bone.</p>
                    <button data-toggle="modal" data-target="#modal-video-baha" type="button" class="gold-btn" title="The Baha System"><span class="glyphicon glyphicon-chevron-right"></span>Learn More About How the Baha System Works</button>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-5 col-sm-offset-1">
                    <img src="/wps/wcm/myconnect/db74cdeb-6dc6-4160-87e5-6d58b4ad37f0/the-baha-system.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-db74cdeb-6dc6-4160-87e5-6d58b4ad37f0-m0jIhNf" alt="The Cochlear Baha System" class="img-responsive">
                </div>
            </div>            
        </div>
    </section>

    <section id="awareness">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>The Baha System helps provide 360-degree sound awareness, improving speech understanding in noise.&sup3;</h2>
                </div>
            </div>            
        </div>
    </section>

    <section id="baha-difference" class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>How the Baha System is different from …</h2>
                    <div class="accordion-header">
                        <h2>CROS Hearing Aids</h2>
                        <div class="accord-indicator">
                            <p>+</p>
                        </div>
                    </div>
                    <div id="cros" class="accordion-body">
                        <div class="col-xs-12 col-sm-10">
                            <ul>
                                <li>The Baha System is a discreet solution worn on one side, while CROS hearing aids require users to wear devices on both ears.</li>
                                <li>The Baha System is an implantable hearing solution designed to use your body’s natural ability to conduct sound through bone conduction.</li>
                                <li>CROS hearing aids are conventional hearing aids placed behind, or inside, each ear.</li>
                                <li>The Baha System bypasses the outer and middle ear and sends clearer, more crisp sound directly to your inner ear.<sup>4</sup></li>
                                <li>CROS hearing aids work by picking up sound on your ‘bad side’ and transmitting it via a hard wire or a wireless signal to your normal hearing ear.</li>
                                <li><strong>Studies show that the Baha System provides better speech understanding in noise than CROS hearing aids.<sup>5</sup></strong></li>
                            </ul>
                        </div>
                    </div>

                    <div class="accordion-header">
                        <h2>Leaving Your Hearing Loss Untreated</h2>
                        <div class="accord-indicator">
                            <p>+</p>
                        </div>
                    </div>
                    <div id="leaving" class="accordion-body">
                        <div class="col-xs-12 col-sm-10">
                           <ul>
                                <li>Research and decades of experience demonstrate the benefits of the Baha System compared to leaving your hearing loss untreated.<sup>6-10</sup> &nbsp; With it, you may be able to:
                                    <ul>
                                        <li>Hear better, even in noisy situations</li>
                                        <li>Enjoy a clean, natural sound, because bypassing your problem reduces the amplification that's needed</li>
                                        <li>Hear sounds as if they are coming from both sides</li>
                                        <li>Become more aware of your surroundings, increasing your ability to hear important sounds</li>
                                        <li>No longer need to keep turning your ’good’ ear toward people when they speak</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <p class="button hidden-xs"><a class="gold-btn" href="baha-implant-system.html"><span class="glyphicon glyphicon-chevron-right"></span>Learn More About the Benefits of the Baha System</a></p>
                    <a href="baha-implant-system.html"><button type="button" class="gold-btn visible-xs" title="The Baha System"><span class="glyphicon glyphicon-chevron-right"></span>Learn More About the Benefits of the Baha System</button></a>
                </div>
            </div>            
        </div>
    </section>

    <section id="testimonials-ssd">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 intro">
                    <h2>Baha Recipient Success Stories.</h2>
                    <p>You are not alone. In fact, each year approximately 60,000 Americans find themselves exactly where you are right now.<sup>11</sup> We hear so many stories from people who struggle with hearing loss in one ear. Luckily, we’ve also heard stories from those who overcame it.</p>
                </div>
                <div class="col-xs-12 bio">
                    <div class="col-xs-12 col-sm-6">
                        <h2>Aaron B., Sudden Hearing Loss</h2>
                        <p>Aaron suddenly lost his hearing in his right ear overnight. Not only did he lose his hearing but he also lost his connection to one of his most favorite things in the world- nature. His hearing loss changed his personality and even made him withdrawal from the ones he loved most. After talking with his audiologist, he knew the Baha 5 SuperPower is exactly what he needed to have 360 degrees of sound again.</p>
                        <div class="col-xs-12">
                            <div class="video visible-xs">
                                <div class="video-holder"></div>
                                <img src="https://img.youtube.com/vi/B41kXtwK8EE/maxresdefault.jpg" alt="How the Baha System Works" class="img-responsive">
                                <div class="btn-play">
                                    <div class="video-id">B41kXtwK8EE</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h2>Debra B., Gradual Hearing Loss From Tumor</h2>
                        <p>Debra was diagnosed with a cholesteatoma that was life changing. The invasive tumor left her totally deaf in her right ear, leaving her without sound and often times missing out on conversations that were happening right in front of her. Thanks to the Baha 5 System, Debra is no longer missing out on any conversations and is enjoying richer relationships with family and friends.</p>
                        <div class="col-xs-12">
                            <div class="video visible-xs">
                                <div class="video-holder"></div>
                                <img src="https://img.youtube.com/vi/rLiYKM5aRpQ/maxresdefault.jpg" alt="How the Baha System Works" class="img-responsive">
                                <div class="btn-play">
                                    <div class="video-id">rLiYKM5aRpQ</div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>                 
                <div class="col-xs-12 videos hidden-xs">
                    <div class="col-xs-12 col-sm-6">
                        <div class="video">
                            <div class="video-holder"></div>
                            <img src="https://img.youtube.com/vi/B41kXtwK8EE/maxresdefault.jpg" alt="How the Baha System Works" class="img-responsive">
                            <div class="btn-play">
                                <div class="video-id">B41kXtwK8EE</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="video">
                            <div class="video-holder"></div>
                            <img src="https://img.youtube.com/vi/rLiYKM5aRpQ/maxresdefault.jpg" alt="How the Baha System Works" class="img-responsive">
                            <div class="btn-play">
                                <div class="video-id">rLiYKM5aRpQ</div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-xs-12">
                     <p class="button hidden-xs"><a href="/wps/wcm/myconnect/us/home/take-the-next-step/cochlear-connections?contentIDR=6509dc30-76e0-4b35-bfb2-ec295c85d750&amp;useDefaultText=0&amp;useDefaultDesc=0" target="_blank" title="" class="gold-btn"><span class="glyphicon glyphicon-chevron-right"></span>Meet Others Who Have Been Where You Are Now</a></p>
                     <a href="/wps/wcm/myconnect/us/home/take-the-next-step/cochlear-connections?contentIDR=6509dc30-76e0-4b35-bfb2-ec295c85d750&amp;useDefaultText=0&amp;useDefaultDesc=0" target="_blank" title=""><button type="button" class="gold-btn visible-xs" title="The Baha System"><span class="glyphicon glyphicon-chevron-right"></span>Meet Others Who Have Been Where You Are Now</button></a>
                </div>
            </div>            
        </div>
    </section>

    <section id="references" class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>REFERENCES</h3>
                    <ol>
                        <li>Wazen JJ, Spitzer JB, Ghossaini SN, Fayad JN, Niparko JK, Cox K, et al. Transcranial contralateral cochlear stimulation in unilateral deafness. Otolaryngology-Head &amp; Neck Surgery 2003;129(3):248-54.</li>
                        <li>Weaver, J. "Single-Sided Deafness: Causes, and Solutions, Take Many Forms." Hearing Journal 68.3 (2015): 20-24. Web. 28 Apr. 2017. http://journals.lww.com/thehearingjournal/Fulltext/2015/03000/Single_Sided_Deafness___Causes,_and_Solutions,.1.aspx</li>
                        <li>Dun, C.A.J. , de Wolf, M.J.F , Wigren, S., Eeg Olofsson, M., Granstrom, G., Green, K., Flynn, M.C., Stalfors, J., Rothera, M., Mylanus, E.A.M., &amp; Cremers, C.W.R.J. (2010) Development and Multi centre Clinical Investigation of a Novel Baha Implant System. Technical and 6 Month Data. Paper presented at CI 2010, Stockholm, Sweden.</li>
                        <li> Gustafsson J. BCDrive performance vs. conventional bone conduction transducer. Cochlear Bone Anchored Solutions AB, 629908, 2015.</li>
                        <li>Niparko JK, Cox KM, Lustig LR. Comparison of the bone-anchored hearing aid implantable hearing device with contralateral routing of offside signal amplification in the rehabilitation of unilateral deafness. Otology &amp; Neurotology, 2003 Jan;24(1):73-78.</li>
                        <li>Flynn MC, Sadeghi A, Halvarsson G. Baha solutions for patients with severe mixed hearing loss. Cochlear Implants Int 2009;10 Suppl 1:43-7.Ð_</li>
                        <li>Hol MK, Snik AF, Mylanus EA, Cremers CW. Long-term results of bone-anchored hearing aid recipients who had previously used air-conduction hearing aids. Arch Otolaryngol Head Neck Surg 2005 Apr;131(4):321-5.</li>
                        <li>Watson GJ, Silva S, Lawless T, Harling JL, Sheehan PZ. Bone anchored hearing aids: a preliminary assessment of the impact on outpatients and cost when rehabilitating hearing in chronic suppurative otitis media. Clin Otolaryngol 2008;33:338–342.</li>
                        <li>Snik AF, Mylanus EA, Proops DW, Wolfaardt J, Hodgetts WA, Somers T, Niparko JK, Wazen JJ, Sterkers O, Cremers CW, Tjellström A. Consensus statements on the Baha system: Where do we stand at present? Ann Otol Rhinol Laryngol 2005 Dec;114(12) Suppl 195:1-12.</li>
                        <li>Lin LM, Bowditch S, Anderson MJ, May B, Cox KM, Niparko K. “Amplification in the rehabilitation of unilateral deafness: speech in noise and directional hearing effects with bone-anchored hearing and contralateral routing of signal amplification.” Otology &amp; Neurotology. 2006;27(2):172-82.</li>
                        <li>Hear-it.org. “Single-Sided Deafness-SSD.” http://www.hear-it.org/Single-sided-deafness. Accessed April 28, 2017.</li>
                    </ol>
                </div>
            </div>            
        </div>
    </section>

    <!-- Modals -->
    <div class="modal fade" id="rfi-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h2>Find Out More</h2>
                </div>
                <div class="modal-body">
                    <form id="mktoForm_9366"> </form>
                    <script>
                        MktoForms2.loadForm("//app-lon04.marketo.com", "087-KYD-238", 9366);
                    </script>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-video-baha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h2>How the Baha System Works</h2>
                </div>
                <div class="modal-body">
                    <div id="player2"></div>
                </div>
            </div>
        </div>
    </div>


    <?php include "includes/footer.php"; ?>

    <script>
        $(document).ready(function() {

            //URL Parsing logic
            var currentURL = window.location.href;
            var parmTab = /tab=/;
            var hasTab = currentURL.search(parmTab);

            if (hasTab >= 0) {
                console.log('has tab parameter');
                var splitURL = currentURL.split('tab=');
                var firstSplitValue = splitURL[1];
                var firstString = firstSplitValue.toString();
                var secondSplit = firstString.split('&');
                var finalProduct = secondSplit[0];

                if (finalProduct == 'avoid' || finalProduct == 'understand' || finalProduct == 'locate') {
                    $("html, body").delay(2000).animate({scrollTop: $('#two-ears').offset().top }, 1000);
                } else {
                    $("html, body").delay(2000).animate({scrollTop: $('#baha-difference').offset().top }, 1000);
                }

                $('#' + finalProduct).show(); 
            }

            //url parameters to scroll to and open accordion

            /* 
            tab=avoid
            tab=understand
            tab=locate
            tab=cros
            tab=leaving
            */
        });
    </script>

<!-- Script for local environment only -->
<script src="js/devAssetPath.js"></script>
</body>
</html>