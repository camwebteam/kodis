
<!DOCTYPE html>
<html lang="en">
<head>
<?php include "includes/header-scripts.php"; ?>
<title>Cochlear Baha Indications | Cochlear </title>
<meta name="description" content="">
<link href="css/mixed.css" rel="stylesheet">
</head>
<body id="indications-mixed">
 <!-- Google Tag Manager -after body tag -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PP9T5H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->

    <?php include "includes/header.php"; ?>

     <section id="hero">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-6 col-lg-5">
                    <h1>Hear what you've been missing.</h1>
                    <p>Conditions like chronic ear infections and draining ears are more than inconvenient, they can cause permanent damage to your middle ear, and may even result in conductive or mixed hearing loss.</p> 
                    <p>While you may be able to hear in quiet environments, you most likely struggle to hear and understand what is being said in noisy ones, making everyday life a bit more challenging.</p>
                </div>
                <div class="col-xs-12">
                    <p class="name">John K., Baha recipient</p>
                </div>
            </div>            
        </div>
    </section>

    <section id="do-you">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2"> 
                    <h2>Do you have conductive or mixed hearing loss?</h2>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                    <p>You might, if you answer "yes" to any of these questions:</p>
                    <ul>
                        <li><span class="glyphicon glyphicon-ok"></span>Do you experience chronic ear infections?</li>
                        <li><span class="glyphicon glyphicon-ok"></span>Do you have draining ears?</li>
                        <li><span class="glyphicon glyphicon-ok"></span>Do you have trouble getting sufficient loudness when using hearing aids?</li>
                        <li><span class="glyphicon glyphicon-ok"></span>Do you battle feedback or distorted sound quality when using hearing aids?</li>
                        <li><span class="glyphicon glyphicon-ok"></span>Do you suffer from sore or irritated ears due to your hearing aids?</li>
                        <li><span class="glyphicon glyphicon-ok"></span>Do you have malformed ears or ear canals?</li>
                    </ul>
                </div>
            </div>            
        </div>
    </section>

    <section id="conductive" class="grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Understanding conductive hearing loss.</h2>
                </div>
                <div class="col-xs-12 col-sm-6"> 
                    <p>Conductive hearing loss occurs when damage to the outer ear or middle ear blocks sound vibrations from reaching your inner ear. People who have conductive hearing loss may feel like their ears are plugged and speech may sound muffled. With this type of hearing loss, you will most likely understand speech, but only if it's loud enough and if there is little to no background noise.</p>

                    <p>Some of the most common causes of conductive hearing loss include:</p>
                    <ul>
                        <li>Skin allergies</li>
                        <li>Malformations at birth</li>
                        <li>Microtia and Astresia</li>
                        <li>Draining ears</li>
                        <li>Chronic ear infections</li>
                        <li>Previous ear surgeries</li>
                        <li>Chronic mastoiditis or middle ear infections</li>
                        <li>Skin growth or cyst (cholesteatoma)</li>
                        <li>Syndromes such as Down, Goldenhar and Treacher Collins</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <img src="/wps/wcm/myconnect/7be9d981-7d39-4f00-be84-0bed8266fd19/understanding-hearing.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-7be9d981-7d39-4f00-be84-0bed8266fd19-m0jAOMa" alt="Understanding conductive hearing loss" class="img-responsive">
                    </ul>
                </div>
            </div>            
        </div>
    </section>

    <section id="mixed">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Understanding mixed hearing loss.</h2>
                </div>
                <div class="col-xs-12 col-sm-6"> 
                    <p>Mixed hearing loss refers to a combination of conductive and sensorineural hearing loss. This means there may be damage in both the outer or middle ear and in the inner ear. People who suffer from mixed hearing loss describe sounds as being both softer and more difficult to understand.</p>

                    <p>Any of the causes of conductive hearing loss plus any of the following can lead to mixed hearing loss:</p>
                    <ul>
                        <li>Aging</li>
                        <li>Exposure to loud noise</li>
                        <li>Head trauma</li>
                        <li>Genetics</li>
                        <li>Otosclerosis</li>
                        <li>M&eacute;ni&egrave;re's disease</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <img src="/wps/wcm/myconnect/7be9d981-7d39-4f00-be84-0bed8266fd19/understanding-hearing.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-7be9d981-7d39-4f00-be84-0bed8266fd19-m0jAOMa" alt="Understanding mixed hearing loss" class="img-responsive">
                    </ul>
                </div>
            </div>            
        </div>
    </section>

    <section id="system" class="gold-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>The Cochlear&trade; Baha<sup>&reg;</sup> System for conductive and mixed hearing loss.</h2>
                </div>
                <div class="col-xs-12 col-sm-6"> 
                    <p>The Cochlear Baha System is an implantable hearing solution that uses your natural ability to conduct sound to bypass the damaged outer and middle ear, and sends clearer, more crisp sound directly to your inner ear.&sup1;</p>
                    <img src="/wps/wcm/myconnect/3effea95-4bab-40de-802a-7430ed2b78df/baha-producs.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-3effea95-4bab-40de-802a-7430ed2b78df-m0jAOMa" alt="Cochlear Baha System" class="img-responsive visible-xs">
                    <button type="button" class="white-btn" title="The Baha System" data-toggle="modal" data-target="#modal-video-baha"><span class="glyphicon glyphicon-chevron-right"></span>Learn More About How the Baha System Works</button>
                </div>
                <div class="col-xs-12 col-sm-6 hidden-xs">
                    <img src="/wps/wcm/myconnect/3effea95-4bab-40de-802a-7430ed2b78df/baha-producs.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-3effea95-4bab-40de-802a-7430ed2b78df-m0jAOMa" alt="Cochlear Baha System" class="img-responsive">
                </div>
            </div>            
        </div>
    </section>

    <section id="baha-difference" class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>How the Baha System is different from...</h2>
                    <div class="accordion-header">
                        <h2>Conventional Hearing Aids</h2>
                        <div class="accord-indicator">
                            <p>+</p>
                        </div>
                    </div>
                    <div id="conventional" class="accordion-body">
                        <div class="col-xs-12 col-sm-10">
                            <ul>
                                <li>The Baha System is an implantable hearing solution designed to use your body's natural ability to conduct sound through bone conduction.</li>
                                <li>Hearing aids try to amplify the sounds you hear by pushing them through the damaged part of the ear.</li>
                                <li>The Baha System bypasses the outer and middle ear and sends clearer, more crisp sound directly to your inner ear.&sup1;</li>
                                <li>Some hearing aids require users to wear an earmold, which can aggravate existing conditions, such as draining ears, or create feedback problems. </li>
                            </ul>
                        </div>
                    </div>

                    <div class="accordion-header">
                        <h2>Middle Ear Surgery</h2>
                        <div class="accord-indicator">
                            <p>+</p>
                        </div>
                    </div>
                    <div id="middle" class="accordion-body">
                        <div class="col-xs-12 col-sm-10">
                           <ul>
                                <li>Surgery is an irreversible treatment option. </li>
                                <li>If you are considering surgery, you can try the Baha System beforehand risk-free. That way, you'll know your options before making your final decision.</li>
                                <li>If you get the Baha System and find it is not right for you, you will have the ability to try other solutions without the risk of losing any remaining hearing you may have.</li>
                                <li>The Baha Implant is designed and tested for long-term reliability and better sound transmission compared to other systems.<sup>2,3</sup></li>
                            </ul>
                        </div>
                    </div>
                    <p class="button hidden-xs"><a class="gold-btn" href="baha-implant-system.html"><span class="glyphicon glyphicon-chevron-right"></span>Learn More About the Benefits of the Baha System</a></p>
                    <a href="baha-implant-system.html"><button type="button" class="gold-btn visible-xs" title="The Baha System"><span class="glyphicon glyphicon-chevron-right"></span>Learn More About the Benefits of the Baha System</button></a>
                </div>
            </div>            
        </div>
    </section>

    <section id="testimonials">
        <div class="container two-video">
            <div class="row">
                <h2>Baha Success Stories</h2>

                <div class="col-xs-12">
                    <div class="col-xs-12 col-sm-6">
                        <p><strong>John K.</strong> lost his hearing in one ear to a tumor. As a successful professional and a busy father of four children, he knew he couldn't let his hearing loss slow him down. Learn more about how the Baha Implant System helped John reconnect with his life and the ones he loves.</p>
                        <div class="col-xs-12 col-sm-6 visible-xs">
                            <div class="video">
                                <div class="video-holder"></div>
                                <img src="https://img.youtube.com/vi/gHy8RgvSkE8/maxresdefault.jpg" alt="How the Baha System Works" class="img-responsive">
                                <div class="btn-play">
                                    <div class="video-id">gHy8RgvSkE8</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <p><strong>Camille S.</strong> was born with Goldenhar Syndrome and also suffered from chronic ear infections and a growth in her middle ear - all by age 11. Camille loved to dance and the Baha Implant System helped her hear the music and enjoy her passions.</p>
                        <div class="col-xs-12 col-sm-6 visible-xs">
                            <div class="video">
                                <div class="video-holder"></div>
                                <img src="https://img.youtube.com/vi/GKNyK9sYrIc/maxresdefault.jpg" alt="How the Baha System Works" class="img-responsive">
                                <div class="btn-play">
                                    <div class="video-id">GKNyK9sYrIc</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 hidden-xs">
                    <div class="col-xs-12 col-sm-6">
                        <div class="video">
                            <div class="video-holder"></div>
                            <img src="/wps/wcm/myconnect/7d19730d-e3d4-4892-bf84-5266053036aa/video-john.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-7d19730d-e3d4-4892-bf84-5266053036aa-m0jAOMa" alt="Baha Success Stories - John" class="img-responsive">
                            <div class="btn-play">
                                <div class="video-id">gHy8RgvSkE8</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="video">
                            <div class="video-holder"></div>
                            <img src="/wps/wcm/myconnect/5022038e-54ef-42ec-aa2b-b48a1348e9b8/video-camille.jpg?MOD=AJPERES&amp;CACHEID=ROOTWORKSPACE-5022038e-54ef-42ec-aa2b-b48a1348e9b8-m0jAOMa" alt="Baha Success Stories - Camille" class="img-responsive">
                            <div class="btn-play">
                                <div class="video-id">GKNyK9sYrIc</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <p class="button hidden-xs"><a href="/wps/wcm/myconnect/us/home/stories-and-community/watch-recipient-videos?contentIDR=d2a8635d-bc93-49e8-9ab1-1bf5af85daaa&amp;useDefaultText=0&amp;useDefaultDesc=0" target="_blank" title="" class="gold-btn"><span class="glyphicon glyphicon-chevron-right"></span>Meet Others Who Have Been Where You Are Now</a></p>
                    <a href="/wps/wcm/myconnect/us/home/stories-and-community/watch-recipient-videos?contentIDR=d2a8635d-bc93-49e8-9ab1-1bf5af85daaa&amp;useDefaultText=0&amp;useDefaultDesc=0" target="_blank" title=""><button type="button" class="gold-btn visible-xs" title="The Baha System"><span class="glyphicon glyphicon-chevron-right"></span>Meet Others Who Have Been Where You Are Now</button></a>
                </div>
            </div>            
        </div>
    </section>

    <section id="references">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>REFERENCES</h3>
                    <ol>
                        <li>Gustafsson J. BCDrive performance vs. conventional bone conduction transducer. Cochlear Bone Anchored Solutions AB, 629908, 2015.</li>
                        <li>Sennerby L, Gottlow J, Rosengren A, Flynn M. An experimental evaluation of a new craniofacial implant using the rabbit tibia model. Part II. Biomechanical findings. Otology and Neurotology (accepted).</li>
                        <li>Dun, C.A.J. , de Wolf, M.J.F , Wigren, S., Eeg Olofsson, M., Granstrom, G.,Green, K., Flynn, M.C., Stalfors, J., Rothera, M., Mylanus, E.A.M., &amp; Cremers, C.W.R.J. (2010) Development and Multi centre Clinical Investigation of a Novel Baha Implant System. Technical and 6 Month Data. Paper presented at CI 2010, Stockholm, Sweden.</li>
                    </ol>
                </div>
            </div>            
        </div>
    </section>

    <!-- Modals -->
    <div class="modal fade" id="rfi-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h2>Find Out More</h2>
                </div>
                <div class="modal-body">
                    <form id="mktoForm_9366"> </form>
                    <script>
                        MktoForms2.loadForm("//app-lon04.marketo.com", "087-KYD-238", 9366);
                    </script>
                </div>
            </div>
        </div>
    </div>

     <div class="modal fade" id="modal-video-baha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h2>How the Baha System Works</h2>
                </div>
                <div class="modal-body">
                    <div id="player1"></div>
                </div>
            </div>
        </div>
    </div>

    <?php include "includes/footer.php"; ?>
 
    <script>
        $(document).ready(function() {

            //URL Parsing logic
            var currentURL = window.location.href;
            var parmTab = /tab=/;
            var hasTab = currentURL.search(parmTab);

            if (hasTab >= 0) {
                console.log('has tab parameter');
                var splitURL = currentURL.split('tab=');
                var firstSplitValue = splitURL[1];
                var firstString = firstSplitValue.toString();
                var secondSplit = firstString.split('&');
                var finalProduct = secondSplit[0];

                $("html, body").delay(2000).animate({scrollTop: $('#baha-difference').offset().top }, 1000);

                $('#' + finalProduct).show(); 
            }
            //console.log('modal script ran');

            //url parameters to scroll to and show accordion

            /* 
            tab=conventional
            tab=middle
            */
        });
    </script>
<!-- Script for local environment only -->
<script src="js/devAssetPath.js"></script>
</body>
</html>