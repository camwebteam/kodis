$(document).ready(function() {

    //Accordion Functionality
    $('.accordion-body').hide();
    
    $('.accordion-header').click(function() {
        //console.log('click');

        if ($(this).next('.accordion-body').css('display') == 'block') {
            //console.log('has block');
            $('.accordion-body').slideUp('fast');
            $('.accord-indicator p').html('+').removeClass('adjust-icon');            
        } else {
            //console.log('does not have block');
            $('.accordion-body').slideUp('fast');
            $('.accord-indicator p').html('+').removeClass('adjust-icon');
             $(this).next('.accordion-body').slideDown('fast');
            $(this).find('.accord-indicator p').html('-').addClass('adjust-icon');
        }
    });

    //video button functionality
    $('.video-holder').hide();

    $('.btn-play').click(function() {

        var videoID = $(this).children('.video-id').html();
        //console.log('video id is ' + videoID);
        $(this).siblings('img').hide();
        $(this).hide();
        $(this).siblings('.video-holder').html('<iframe width="100%" height="315" src="https://www.youtube.com/embed/' + videoID + '?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>').show();
    }); 

    //Mobil nav functionality
    $('.nav-closed').click(function(){
        $('.mobile-nav-container').addClass('open').removeClass('closed');
    });

    $('.nav-open').click(function(){
        $('.mobile-nav-container').addClass('closed').removeClass('open')
    });

});

//YouTube video logic
//Call for YouTube API
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//YouTube Players - one per youtube video
var player1; 
var player2;
var player3;
var player4;
var player5;
var player6;
var player7;
var player8;
var player9;
var player10;

//play video configuration
function onYouTubeIframeAPIReady() {
    player1 = new YT.Player('player1', {
        height: '315',
        width: '560',
        videoId: '1IdcXl5gBvM',
    });

    player2 = new YT.Player('player2', {
        height: '315',
        width: '100%',
        videoId: 'EZ-M9AxesVc',
     });

    player3 = new YT.Player('player3', {
        height: '315',
        width: '100%',
        videoId: 'osK45mSliHs',
     });

    player4 = new YT.Player('player4', {
        height: '315',
        width: '100%',
        videoId: 'CpH8yedRuOY',
     });

    player5 = new YT.Player('player5', {
        height: '315',
        width: '100%',
        videoId: 'O7o3gdL0aeg',
     });

    player6 = new YT.Player('player6', {
        height: '315',
        width: '100%',
        videoId: 'yZDGlGb9kNE',
     });

    player7 = new YT.Player('player7', {
        height: '315',
        width: '100%',
        videoId: 'i8j85R0-ko0',
     });

    player8 = new YT.Player('player8', {
        height: '315',
        width: '100%',
        videoId: '1QU7ezUKdPg',
     });

    player9 = new YT.Player('player9', {
        height: '315',
        width: '100%',
        videoId: 'ndKhdVTPP2A',
     });

    player10 = new YT.Player('player10', {
        height: '315',
        width: '100%',
        videoId: 'B41kXtwK8EE',
     });

    //Functionality to stop video when modal window is closed from either the close button or the modal gray background
    $('#modal-video-baha button.close, #modal-video-baha').click(function () {
        player1.pauseVideo();
        player2.pauseVideo();
    });
}//end api function

//console.log('indications script ran today');