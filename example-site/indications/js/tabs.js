
$(document).ready(function() {

    $('.tab-btn').click(function(){

        $('.tab-btn').removeClass('active');
        $(this).addClass('active');
        $('.tab-btn .indicators').html('+');
        $(this).find('.indicators').html('-');

        if ($(this).hasClass('baha-5')) {
            $('.tab-content').hide()
            $('.baha-5-content').fadeIn('fast');
        } else if ($(this).hasClass('baha-5-power')) {
            $('.tab-content').hide()
            $('.baha-5-power-content').fadeIn('fast');
        } else {
            $('.tab-content').hide()
            $('.baha-5-sp-content').fadeIn('fast');
        }
    })
});  

//console.log('tabs script ran today');